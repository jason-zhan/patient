const express = require('express')
const mysql = require('mysql')
const app = express()
var bodyParser = require('body-parser')

var connsql = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: '12345678',
    database: 'clinic'

})
connsql.connect()

app.engine('html', require('express-art-template'))
app.use('/public/', express.static('./public/'))

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/', (req, res)=> {
    // res.sendFile( __dirname +"/views/"+ "index.html" );
    res.render('index.html')
})

app.get('/Succ', (req, res)=> {
    res.render('Succ.html')
})

app.get('/Error', (req, res)=> {
    res.render('Error.html')
})

app.get('/adminlogin', (req, res)=> {
    res.render('adminlogin.html')
})

app.get('/patientlist', (req, res)=> {
    // res.sendFile( __dirname +"/views/"+ "register.html" );
    var loginsql = "select id from UserInfo where isLogin = 1"
    connsql.query(loginsql,  (err, result)=> {
        if (err) {
            console.log('err System:', err)
            res.render('Error.html')
            return
        }
        if (result == '') {
            console.log('Please login first!')
            res.render('Error.html')
            //res.json({ code: 403, msg: 'Please login first' })

        } else {
            console.log('Login Succ！')
            res.render('patientlist.html')
        }
    })
})

app.use('/patient/all',(req, res) => {
    console.log('enter get patient information ...')
    var patientSql = "select a.id as id, name, username as photoId, email, mobile, birthday, address1, appoint_date from  patient_info a, appoint_info b where a.username = b.patient_id"
    connsql.query(patientSql,  (err, result)=> {
        if (err) {
            console.log('err System:', err)
            return
        }
        if (result == '') {
            console.log('There have no patient infomation！')
            res.render('Error.html')
            //res.json({ code: -1, msg: 'There have no patient infomation！' })

        } else {
            console.log(result)
            res.json({ code: 200, msg: result })

        }
    })
})

app.use('/logout',(req, res) => {
    var updateLog = "update UserInfo set isLogin = 0"
    connsql.query(updateLog,  (err, result)=> {
        if (err) {
            console.log('err System:', err)
            return
        }
        if (result == '') {
            console.log('There have no patient infomation！')
            res.json({ code: -1, msg: 'There have no patient infomation！' })

        } else {
            console.log(result)
            res.json({ code: 200, msg: result })

        }
    })
})

app.use('/login', (req, res)=> {
    var login = {
        "user": req.body.user,
        "pwd": req.body.pwd
    }

    var loginsql = "select UserName,Password from UserInfo where UserName='" + login.user + "'and Password='" + login.pwd + "'"
    connsql.query(loginsql,  (err, result)=> {
        if (err) {
            console.log('err System:', err)
            return
        }
        if (result == '') {
            console.log('User name and password is incorrect！')
            res.json({ code: -1, msg: 'User name and password is incorrect！' })

        } else {
            console.log('Login Succ！')
            res.json({ code: 200, msg: 'Login Succ!' })

        }
    })
    var updateLog = "update UserInfo set isLogin = 1"
    connsql.query(updateLog,  (err, result)=> {})
})

app.use('/regs', (req, res) => {
    var regs = {
        "user": req.body.user,
        "pwd": req.body.pwd,
        "username": req.body.username,
        "email": req.body.email,
        "birthDay": req.body.birthDay,
        "appointDate":req.body.appointTime,
        "phone":req.body.phone,
        "address":req.body.address
    }
    console.log('enter register...')
    console.log(regs)
    if(regs.birthDay =='') {
        regs.birthDay = "2020-01-01"
    }
    if(regs.appointDate =='') {
        regs.appointDate = "2020-01-01"
    }

    var regssql = "insert into patient_info(username,password, email, mobile, birthday, address1, name) values('" + regs.username + "','" + 
    regs.pwd + "','" + regs.email + "','" + regs.phone + "','"+regs.birthDay + "','"+regs.address + "','"+regs.user +  "')";
    var appointSql = "insert into appoint_info(patient_id, appoint_date) values ('" + regs.username + "','"+regs.appointDate +  "')";
    var selsql = "select username from patient_info where username='" + regs.username + "'"
    function regfun() {
        connsql.query(regssql,  (err, result)=> {
            if (err) {
                console.log(err);
                return 
            }
            console.log('Register Succ.')
        })
        connsql.query(appointSql,  (err, result)=> {
            if (err) {
                console.log(err);
                return 
            }
            console.log('appoint Succ.')
        })
        res.json({ code: 200, msg: 'Register Succ!' })
    }
    connsql.query(selsql,  (err, result)=>{
        if (err) {
            console.log(err)
            return
        }
        if (result == '') {
            regfun() 
        } else { 
            res.json({code:403, msg:"You have already registered."})
            console.log(regs.user + ', You have already registered.')
       }

    })
})

app.listen(3001)

// connsql.end()

